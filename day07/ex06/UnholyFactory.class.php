<?php

class UnholyFactory
{
    private static $absorbed = array();
    private static $fabricated = array();

    public function absorb($obj)
    {
        if (!$obj instanceof Fighter)
            print ("(Factory can't absorb this, it's not a fighter)" . PHP_EOL);
        else if (in_array($obj, self::$absorbed))
        {
            if (get_class($obj) == "Footsoldier")
                $name = "foot soldier";
            else if (get_class($obj) == "Archer")
                $name = "archer";
            else if (get_class($obj) == "Assassin")
                $name = "assassin";
            else 
                $name = get_class($obj);
            print("(Factory already absorbed a fighter of type $name)" . PHP_EOL);
        }
        else
        {
            if (get_class($obj) == "Footsoldier")
                $name = "foot soldier";
            else if (get_class($obj) == "Archer")
                $name = "archer";
            else if (get_class($obj) == "Assassin")
                $name = "assassin";
            else 
                $name = get_class($obj);
            self::$absorbed[$name] = $obj;
            print("(Factory absorbed a fighter of type $name)" . PHP_EOL);
        }
    }

    public function fabricate($str)
    {
        if (!(array_key_exists($str, self::$absorbed)))
        {
            print ("(Factory hasn't absorbed any fighter of type $str)" . PHP_EOL);
            return (null);
        }
        else
        {
            self::$fabricated[] = self::$absorbed[$str];
            print ("(Factory fabricates a fighter of type $str)" . PHP_EOL);
            return (self::$absorbed[$str]);
        }
    }

    public function fight($str_enemy)
    {
        $this->fight($str_enemy);
    }

}

?>