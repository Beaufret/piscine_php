<?php

class NightsWatch
{
    private static $members = array();

    static public function recruit($player)
    {
        self::$members[] = $player;
    }

    public function fight()
    {
        foreach (self::$members as $elem)
        {
            if ($elem instanceof IFighter)
                echo $elem->fight();
        }
    }
}

?>