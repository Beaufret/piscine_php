<?php

class Jaime extends Lannister
{
    public function sleepWith($perso)
    {
        if (!($perso instanceof Lannister))
            return print("Let's do this.".PHP_EOL);
        else if ($perso instanceof Cersei)
            return print("With pleasure, but only in a tower in Winterfell, then.".PHP_EOL);
        else
            return print("Not even if I'm drunk !".PHP_EOL);
    }
}
?>