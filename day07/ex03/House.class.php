<?php

class House
{
    public function introduce()
    {
        $housename = static::getHouseName();
        $houseseat = static::getHouseSeat();
        $housemotto = static::getHouseMotto();
        echo "House ".$housename." of ".$houseseat." : \"".$housemotto."\"\n";
    }
}

?>