SELECT title AS Title, summary AS Summary, prod_year 
FROM db_rbeaufre.film 
INNER JOIN db_rbeaufre.genre 
ON film.id_genre = genre.id_genre AND genre.name = 'erotic' 
ORDER BY prod_year DESC;