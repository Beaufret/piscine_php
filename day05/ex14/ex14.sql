SELECT floor_number AS floor, SUM(nb_seats) AS nb_seats
FROM db_rbeaufre.cinema
GROUP BY floor_number
ORDER BY SUM(nb_seats) DESC;