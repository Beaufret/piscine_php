SELECT `db_rbeaufre`.`film`.`title`, `db_rbeaufre`.`film`.`summary` 
FROM `db_rbeaufre`.`film` 
WHERE LOWER(summary) 
LIKE '%vincent%' 
ORDER BY id_film ASC;