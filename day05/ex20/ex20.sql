SELECT film.id_genre AS 'id_genre', genre.name AS name_genre, film.id_distrib AS id_distrib, distrib.name AS name_distrib, title as title_film 
FROM db_rbeaufre.film
LEFT JOIN db_rbeaufre.genre ON genre.id_genre = film.id_genre
LEFT JOIN db_rbeaufre.distrib ON distrib.id_distrib = film.id_distrib
WHERE film.id_genre >= 4
AND film.id_genre <= 8
AND film.id_genre IS NOT NULL
AND film.id_distrib IS NOT NULL
AND genre.id_genre IS NOT NULL
AND distrib.id_distrib IS NOT NULL;