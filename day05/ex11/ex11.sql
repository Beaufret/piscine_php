SELECT UPPER(db_rbeaufre.user_card.last_name) AS NAME, first_name, price
FROM db_rbeaufre.member
INNER JOIN db_rbeaufre.user_card
ON db_rbeaufre.user_card.id_user = db_rbeaufre.member.id_user_card
INNER JOIN db_rbeaufre.subscription 
ON subscription.id_sub = member.id_sub 
WHERE subscription.price > 42
ORDER BY last_name ASC, first_name ASC;