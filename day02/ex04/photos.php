#!/usr/bin/php
<?php
    if ($argc != 2)
        return ;
    else
    {
        $input = $argv[1];
        if (preg_match('#^.*\.[0-9a-zA-Z_.~-]*\.[a-zA-Z]{2,3}\/?.*$#', $input) != 1 && preg_match('#^http:\/\/.*\.[0-9a-zA-Z_.~-]*\.[a-zA-Z]{2,3}\/?.*$#', $input) != 1 && preg_match('#^https:\/\/.*\.[0-9a-zA-Z_.~-]*\.[a-zA-Z]{2,3}\/?.*$#', $input) != 1)
        {
            echo "Bad Website Format - Failure\n";
            return;
        }
        else
        {
            if (preg_match('#http://#', $input) != 1 && preg_match('#^www\.[0-9a-zA-Z_.~-]*\.[a-zA-Z]{2,3}\/?$#', $input) == 1)
                $input = "https://$input";
            $url = preg_replace('#http:#', "https:", $input);
            $curl1 = curl_init($url);
            curl_setopt($curl1, CURLOPT_RETURNTRANSFER, TRUE);
            $page = curl_exec($curl1);
            $path = strstr($argv[1], "www");
            if (preg_match("/\//", $path))
                $path = strstr($path, "/", 1);
            if (!is_dir($path))
                mkdir ($path);
            $link = "https://".$path;
            $ar1 = preg_split('#(?=<)|(?<=>)#', $page, 0, PREG_SPLIT_NO_EMPTY);
            foreach ($ar1 as $key1 => $elem1)
            {
                $preg1 = preg_match("#^<img.*>$#", "$elem1");
                if ($preg1 != 1)
                    unset($ar1[$key1]);
            }
            foreach ($ar1 as $key1 => $elem1)
            {
                $ar2 = preg_split('#(?<=src=["\'])#', $elem1, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                $ar2 = preg_split('#["\']#', $ar2[1], 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                if (preg_match('#.*src="http.*#', $elem1) == 1 || preg_match('#.*src=\'http.*#', $elem1) == 1)
                {
                    $ar3 = preg_split('#/#', $ar2[0], 0, PREG_SPLIT_NO_EMPTY);
                    $count3 = count($ar3);
                    $file_name = $ar3[$count3 - 1];
                    $saveto = "./$path/$file_name";
                    $fp = fopen($saveto, "w");
                    //echo "cat1---".$key1."--".$ar2[0]."\n";
                    $curl2 = curl_init($ar2[0]);
                    curl_setopt($curl2, CURLOPT_FILE, $fp);
                    curl_exec($curl2);
                    curl_close($curl2);
                    fclose($fp);
                }
                else if (preg_match('#.*src="\/?.*#', $elem1) == 1 || preg_match('#.*src=\'\/?.*#', $elem1) == 1)
                {
                    $ar3 = preg_split('#/#', $ar2[0], 0, PREG_SPLIT_NO_EMPTY);
                    $count3 = count($ar3);
                    $file_name = $ar3[$count3 - 1];
                    $saveto = "./$path/$file_name";
                    $fp = fopen($saveto, "w");
                    if (preg_match('#.*src="\/.*#', $elem1) == 1)
                    {
                        $tocurl = "https://$path$ar2[0]";
                    }
                    else
                    {
                        $tocurl = "https://$path/$ar2[0]";
                    }
                    //echo "cat2---".$key1."--".$tocurl."\n";
                    $curl2 = curl_init($tocurl);
                    curl_setopt($curl2, CURLOPT_FILE, $fp);
                    curl_exec($curl2);
                    curl_close($curl2);
                    fclose($fp);
                }
            }
            curl_close($curl1);
        }
    }
?>
