#!/usr/bin/php
<?php
$handle = fopen("/var/run/utmpx", "r");
$contents = fread($handle, filesize("/var/run/utmpx"));
$sub = substr($contents, 1256);
$pattern = 'a256user/a4id/a32line/ipid/itype/I2time/a256host/i16pad';
$usr = get_current_user();
$i = 0;
$tab2 = array();
date_default_timezone_set('Europe/paris');
while ($sub)
{
    $tab = unpack($pattern, $sub);
    //print_r($tab);
	if (strcmp(trim($tab[user]), $usr) == 0)
	{
		if ($tab[type] == 7)
			$tab2[$i] = trim($tab[user])." ".trim($tab[line])."  ".date("M j H:i", $tab[time1]);
	}
	$i = $i + 1;
	$sub = substr($sub, 628);
}
fclose($handle);
sort($tab2);
foreach ($tab2 as $elem)
	echo $elem." \n";
?>