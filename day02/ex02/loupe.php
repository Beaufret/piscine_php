#!/usr/bin/php
<?php
    if ($argc < 2 || file_exists($argv[1]) == FALSE)
    {
        return ;
    }
    else
    {
        $raw = file_get_contents($argv[1]);
        $ar1 = preg_split('#(?=<)|(?<=>)#', $raw, 0, PREG_SPLIT_NO_EMPTY);
        $len = count($ar1);
        $i = 0;
        while ($i < $len)
        {
            if (preg_match('#^<a href.*>$#', $ar1[$i]) == 1)
            {
                if (preg_match('#title="#', $ar1[$i]) == 1)
                {
                    $ar3 = preg_split('#(title=")|(")#', $ar1[$i], 0, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
                    foreach ($ar3 as $key3 => $value3)
                    {
                        if (preg_match('#^title="$#', $ar3[$key3]) == 1 && array_key_exists($key3 + 1, $ar3))
                        {
                            $ar3[$key3 + 1] = strtoupper($ar3[$key3 + 1]);
                        }
                    }
                    $ar1[$i] = implode($ar3);
                }
                $j = $i + 1;
                while ($j < $len && preg_match('#^</a>$#', $ar1[$j]) != 1)
                {
                    if (preg_match('#^<.*>$#', $ar1[$j]) != 1)
                        $ar1[$j] = strtoupper($ar1[$j]);
                    else if (preg_match('#^<img.*title=".*".*>$#', $ar1[$j]) == 1 || preg_match('#^<img.*alt=".*".*>$#', $ar1[$j]) == 1)
                    {
                        if (preg_match('#title=".*"#', $ar1[$j]) == 1)
                        {
                            $ar2 = preg_split('#(title=")|(")#', $ar1[$j], 0, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
                            foreach ($ar2 as $key => $value)
                            {
                                if (preg_match('#^title="$#', $ar2[$key]) == 1 && array_key_exists($key + 1, $ar2))
                                    $ar2[$key + 1] = strtoupper($ar2[$key + 1]);
                            }
                            $ar1[$j] = implode($ar2);
                        }
                        if (preg_match('#alt=".*"#', $ar1[$j]) == 1)
                        {
                            $ar4 = preg_split('#(alt=")|(")#', $ar1[$j], 0, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
                            foreach ($ar4 as $key4 => $value4)
                            {
                                if (preg_match('#^alt="$#', $ar4[$key4]) == 1 && array_key_exists($key4 + 1, $ar4))
                                    $ar4[$key4 + 1] = strtoupper($ar2[$key4 + 1]);
                            }
                            $ar1[$j] = implode($ar4);
                        }
                    }
                    else if (preg_match('#^<span.*>$#', $ar1[$j]) == 1)
                    {
                        $ar6 = preg_split('#(title=")|(")#', $ar1[$j], 0, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
                        foreach ($ar6 as $key6 => $value6)
                        {
                            if (preg_match('#^title="$#', $ar6[$key6]) == 1 && array_key_exists($key6 + 1, $ar6))
                            $ar6[$key6 + 1] = strtoupper($ar2[$key6 + 1]);
                        }
                        $ar1[$j] = implode($ar6);
                    }
                    $j = $j + 1;
                }
            }
            $i = $i + 1;
        }
        $res = implode($ar1);
        echo $res;
    }
?>