#!/usr/bin/php
<?php

    function ft_check($da)
    {
        if (count($da) != 5)
            return (0);
        if (preg_match('#^[Ll]undi$|^[Mm]ardi$|^[Mm]ercredi$|^[Jj]eudi$|^[Vv]endredi$|^[Ss]amedi$|^[Dd]imanche$#', $da[0]) != 1)
            return (0);
        if (preg_match('#^[0-9]{1,2}$#', $da[1]) != 1)
            return (0);
        if (preg_match('#^[Jj]anvier$|^[Ff][ée]vrier$|^[Mm]ars$|^[Aa]vril$|^[Mm]ai$|^[Jj]uin$|^[Jj]uillet$|^[Aa]o[uû]t$|^[Ss]eptembre$|^[Oo]ctobre$|^[Nn]ovembre$|^[Dd][ée]cembre$#', $da[2]) != 1)
            return (0);
        if (preg_match('#^[0-9]{4}$#', $da[3]) != 1)
            return (0);
        if (preg_match('#^[0-9]{2}:[0-9]{2}:[0-9]{2}$#', $da[4]) != 1)
            return (0);
        return (1);
    }

    if ($argc == 1)
        return ;
    else
    {
        $da = preg_split('# #', $argv[1]);
        if (ft_check($da) != 1)
        {
            echo "Wrong Format"."\n";
            return;
        }
        else 
        {
            $ca = array("", "janvier", "fevrier", "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre", "decembre");
            $da[2] = array_search(strtolower($da[2]), $ca);
            date_default_timezone_set('Europe/Paris');
            echo (strtotime("$da[2]/$da[1]/$da[3] $da[4]"))."\n";
        }
    }
?>