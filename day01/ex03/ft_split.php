<?php
    function ft_split($str)
    {
        $res = preg_split('/ +/', trim($str));
        if (empty($res))
            return null;
        sort($res);
        return ($res);
    }
?>