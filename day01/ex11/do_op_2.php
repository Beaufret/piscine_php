#!/usr/bin/php
<?php
    function ft_calc($a, $op, $b)
    {
        if ($op == "+")
            return ($a + $b);
        if ($op == "-")
            return ($a - $b);
        if ($op == "*")
            return ($a * $b);
        if ($op == "/" && $b != 0)
            return ($a / $b);
        if ($op == "%" && $b != 0)
            return ($a % $b);    
    }
    function ft_check_num($a)
    {
        return (ctype_digit($a));
    }
    function ft_check_op($a)
    {
        if ('$a' == '+' || '$a' == '-' || '$a' == '/' || '$a' == '%' || '$a' == '*')
            return (true);
        else
            return (false);
    }
    if ($argc != 2)
    {
        echo "Incorrect Parameters"."\n";
        return;
    }
    else
    {
        $trans = array("+" => " + ", "-" => " - ", "/" => " / ", "%" => " % ", "*" => " * ");
        $str = strtr($argv[1], $trans);
        $array = preg_split('/ +/', trim($str));
        $a = trim($array[0]);
        if (ft_check_num($a) == false)
        {
            echo "Syntax Error"."\n";
            return;
        }
        $op = trim($array[1]);
        if (ft_check_op($op) != 0)
        {
            echo "Syntax Error"."\n";
            return;
        }
        $b = trim($array[2]);
        if (ft_check_num($b) == false || (("$b" == 0) && ("$op" == '/' || "$op" == '%')))
        {
            echo "Syntax Error"."\n";
            return;
        }
        echo ft_calc($a, $op, $b)."\n";
    }
?>