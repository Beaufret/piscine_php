#!/usr/bin/php
<?php
    if ($argc == 1)
    {
        return;
    }
    else
    {
        array_shift($argv);
        foreach ($argv as $elem)
        {
            if(is_array($res))
                $res = array_merge(preg_split('/ +/', trim($elem)), $res);
            else
                $res = preg_split('/ +/', trim($elem));
        }
        sort($res);
        foreach ($res as $elem)
        {
            echo $elem;
            echo "\n";
        }
    }
?>