#!/usr/bin/php
<?php
    function ft_calc($a, $op, $b)
    {
        if ($op == "+")
            return ($a + $b);
        if ($op == "-")
            return ($a - $b);
        if ($op == "*")
            return ($a * $b);
        if ($op == "/" && $b != 0)
            return ($a / $b);
        if ($op == "%" && $b != 0)
            return ($a % $b);    
    }
    if ($argc != 4)
    {
        echo "Incorrect Parameters"."\n";
        return;
    }
    else
    {
        array_shift($argv);
        $a = trim($argv[0]);
        $op = trim($argv[1]);
        $b = trim($argv[2]);
        echo ft_calc($a, $op, $b)."\n";
    }
?>