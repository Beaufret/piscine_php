#!/usr/bin/php
<?php
    function ft_is_even($num)
    {
        if (!is_numeric($num))
            return "'$num' n'est pas un chiffre\n";
        else if ($num % 2 == 0)
            return "Le chiffre $num est Pair\n";
        else 
            return "Le chiffre $num est Impair\n";
    }
    
    echo "Entrez un nombre: ";
    while(1)
    {
        $f = trim(fgets(STDIN));
        if (feof(STDIN))
        {
            echo "\n";
            return;
        }
        else
        {
            echo ft_is_even($f);
            echo "Entrez un nombre: ";
        }
    }
    echo "\n";
?>