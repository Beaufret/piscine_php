#!/usr/bin/php
<?php
    if ($argc < 3)
    {
        return;
    }
    else
    {
        array_shift($argv);
        $toto = $argv[0];
        array_shift($argv);
        $arr = array();
        foreach($argv as $elem)
        {
            $tmp_array = preg_split('/:/', $elem);
            $arr[$tmp_array[0]] = $tmp_array[1];
        }
        if (array_key_exists($toto, $arr))
            echo $arr[$toto]."\n";
    }
?>