#!/usr/bin/php
<?php
    if ($argc == 1)
    {
        return;
    }
    else
    {
        
        $str = trim($argv[1]);
        $str = preg_replace('/\s+/', ' ', $str);
        $tab = preg_split('/ +/', $str);
        if (count($tab) == 1 && $tab[0] == "")
            return;
        else if (count($tab) == 1)
        {
            echo $tab[0]."\n";
            return;
        }
        else
        {
            $last = array_shift($tab);
            foreach($tab as $elem)
            {
                echo $elem;
                echo " ";
            }
            echo $last;
            echo "\n";
        }
    }
?>