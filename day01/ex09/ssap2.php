#!/usr/bin/php
<?php
    if ($argc == 1)
    {
        return;
    }
    else
    {
        array_shift($argv);
        $tab = array();
        foreach ($argv as $elem)
        {
            if(is_array($tab))
                $tab = array_merge(preg_split('/ +/', trim($elem)), $tab);
            else
                $tab = preg_split('/ +/', trim($elem));
        }
        $tab2 = array();
        $tab3 = array();
        foreach ($tab as $elem)
        {
            if (!ctype_alpha($elem))
            {
                $tab2[] = $elem;
            }
        }
        foreach ($tab2 as $elem)
        {
            if (!ctype_digit($elem))
            {
                $tab3[] = $elem;
            }
        }
        $tab = array_diff($tab, $tab2);
        natcasesort($tab);
        $tab2 = array_diff($tab2, $tab3);
        sort($tab2, SORT_STRING);
        sort($tab3, SORT_STRING);
        foreach ($tab as $elem)
        {
            echo $elem;
            echo "\n";
        }
        foreach ($tab2 as $elem)
        {
            echo $elem;
            echo "\n";
        }
        foreach ($tab3 as $elem)
        {
            echo $elem;
            echo "\n";
        }
    }
?>