function init_at_load()
{
    if(check_cookie())
    {
        cookie = JSON.parse(check_cookie());
        var ft_list = document.getElementById("ft_list");
        var i = 0;
        while (i < cookie.length)
        {
            var input_node = document.createTextNode(cookie[i]);
            var new_elem = document.createElement('div');
            new_elem.appendChild(input_node);
            new_elem.setAttribute("class", "elems");
            if (i % 2 == 0)
                new_elem.style.backgroundColor = "sandybrown";
            ft_list.appendChild(new_elem);
            i++;
        }
    }
    delete_elems();
}

function check_cookie() 
{
    var name = "cookie_with_to_do_list=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    var i = 0;
    while (i < ca.length)
    {
        var c = ca[i];
        while (c.charAt(0) == ' ') 
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
        i++;
    }
    return "";
}

function create_new()
{
    var ft_list = document.getElementById("ft_list");
    
    var new_elem = document.createElement('div');
    new_elem.setAttribute("class", "elems");
    
    var input = prompt("New element :");
    if (input === null || input === "" || !input.trim())
        return;
    var input_node = document.createTextNode(input);
    new_elem.appendChild(input_node);

    var array = new Array();

    if(ft_list.childElementCount === 0)
    {
        ft_list.appendChild(new_elem);
        var elems = document.getElementsByClassName('elems');
        var i = 0;
        while (i < elems.length)
        {
            array.push(elems[i].innerText);
            i++;
            if (i % 2 == 0)
                new_elem.style.backgroundColor = "sandybrown";
        }
        var cookie_list = JSON.stringify(array);
        setCookie("cookie_with_to_do_list", cookie_list);
        var j = 0;
        var elemslol = document.getElementsByClassName("elems");
        while (j < elems.length)
        {
            if (j % 2 == 0)
                elemslol[j].style.backgroundColor = "sandybrown";
            else 
                elemslol[j].style.backgroundColor = "rosybrown";
            j++;
        }
    }
    else 
    {
        ft_list.insertBefore(new_elem, ft_list.firstChild);
        var elems = document.getElementsByClassName('elems');
        var i = 0;
        while (i < elems.length)
        {
            array.push(elems[i].innerText);
            i++;
            if (i % 2 == 0)
                new_elem.style.backgroundColor = "sandybrown";
        }
        var cookie_list = JSON.stringify(array);
        setCookie("cookie_with_to_do_list", cookie_list);
        var j = 0;
        var elemslol = document.getElementsByClassName("elems");
        while (j < elems.length)
        {
            if (j % 2 == 0)
                elemslol[j].style.backgroundColor = "sandybrown";
            else 
                elemslol[j].style.backgroundColor = "rosybrown";
            j++;
        }
    }
    delete_elems();
}

function delete_elems()
{
    var elems = document.getElementsByClassName("elems");
    var i = 0;
    while (i < elems.length)
    {
        elems[i].onclick = function(){
            if (window.confirm(`Are you sure ?`))
            {
                this.parentNode.removeChild(this);
                array = [];
                var i = 0;
                while (i < elems.length)
                {
                    array.push(elems[i].innerText);
                    i++;
                }
                var cookie_list = JSON.stringify(array);
                setCookie("cookie_with_to_do_list", cookie_list);
                var j = 0;
                var elemslol = document.getElementsByClassName("elems");
                while (j < elems.length)
                {
                    if (j % 2 == 0)
                        elemslol[j].style.backgroundColor = "sandybrown";
                    else 
                        elemslol[j].style.backgroundColor = "rosybrown";
                    j++;
                }
            }
            else
                return ;
        }
        i++;
    }
}

function setCookie(cname, cvalue) 
{
    var date = new Date();
    date.setTime(date.getTime() + (30*24*60*60*1000));
    var expires = "expires="+ date.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=./";
}