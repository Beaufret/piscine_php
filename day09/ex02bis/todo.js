function init_at_load()
{
    $('#ft_list').html(decodeURIComponent(check_cookie()));
}

function check_cookie() 
{
    var name = "cookie_with_to_do_list=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    var i = 0;
    while (i < ca.length)
    {
        var c = ca[i];
        while (c.charAt(0) == ' ') 
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
        i++;
    }
    return "";
}

$('#button').click(function() {
    var input = prompt("New element :");
    if (input === null || input === "" || !input.trim())
        return;
    $('#ft_list').prepend(`<div class='elems' onclick=delete_elems(this)>${input}</div>`);
    var html_content = $("#ft_list").html();
    setCookie('cookie_with_to_do_list', encodeURIComponent(html_content));
});

function delete_elems(elem)
{
    if (window.confirm(`Are you sure ?`))
    {
		$(elem).remove();
        var html_content = $("#ft_list").html();
        setCookie('cookie_with_to_do_list', encodeURIComponent(html_content));
    }
    else
        return;
}


function setCookie(cname, cvalue) 
{
    var date = new Date();
    date.setTime(date.getTime() + (30*24*60*60*1000));
    var expires = "expires="+ date.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=./";
}