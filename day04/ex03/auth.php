<?php
function auth($login, $passwd)
{
    $dir = $_SERVER["DOCUMENT_ROOT"]."/private";
    $file = "passwd";
    $path = $dir."/".$file;
    if (!file_exists($dir) || !file_exists($path))
    {  
        return (FALSE);
    }
    else
    {
        if (!($account = unserialize(file_get_contents($path))))
        {
            return (FALSE);
        }
        else
        {
            $pw_h = hash("whirlpool", $passwd);
            foreach ($account as $key => $user)
            {
                if ($user["login"] == $login)
                {
                    if ($user["passwd"] == $pw_h)
                    {
                        $flag = 1;
                        return (TRUE);
                        exit;
                    }
                }
            }
            return (FALSE);
        }
    }
}
?>