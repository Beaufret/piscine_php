<?php
function ft_create_account($login, $pw)
{
    $dir = $_SERVER["DOCUMENT_ROOT"]."/private";
    if (!file_exists($dir))
        mkdir($dir);
    $file = "passwd";
    $path = $dir."/".$file;
    if (!file_exists($path))
    {
        $account[] = array("login" => $login, "passwd" => $pw);
        $account2[] = serialize($account);
        file_put_contents($path, $account2);
        echo "OK\n";
    }
    else if ($account = unserialize(file_get_contents($path)))
    {
        foreach ($account as $key => $user)
        {
            if ($user["login"] == $_POST["login"])
            {
                echo "ERROR\n";
                exit (0);
            }

        }
        $account[] = array("login" => $login, "passwd" => $pw);
        $account2[] = serialize($account);
        file_put_contents($path, $account2);
        echo "OK\n";
    }
}

if ($_POST["login"] && $_POST["passwd"] && ($_POST["submit"] == "OK"))
{
    $login = $_POST['login'];
    $pw = hash("whirlpool", $_POST['passwd']);
    ft_create_account($login, $pw);
}
else
    echo "ERROR\n";
?>