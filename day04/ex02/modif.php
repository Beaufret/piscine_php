<?php
function ft_modify_account($login, $oldpw, $newpw)
{
    $dir = $_SERVER["DOCUMENT_ROOT"]."/private";
    $file = "passwd";
    $path = $dir."/".$file;
    if (!file_exists($dir) || !file_exists($path))
    {  
        echo "ERROR\n";
        exit ;
    }
    else if ($account = unserialize(file_get_contents($path)))
    {
        $flag = 0;
        foreach ($account as $key => $user)
        {
            if ($user["login"] == $_POST["login"])
            {
                if ($user["passwd"] == $oldpw)
                {
                    $account[$key]["passwd"] = $newpw;
                    $flag = 1;
                }
            }
        }
        if ($flag == 0)
        {  
            echo "ERROR\n";
            exit ;
        }
        else if ($flag == 1)
        {
            $account2[] = serialize($account);
            file_put_contents($path, $account2);
            echo "OK\n";
        }
    }
}

if ($_POST["login"] && $_POST["oldpw"] && $_POST["newpw"] && ($_POST["submit"] == "OK"))
{
    $login = $_POST['login'];
    $oldpw = hash("whirlpool", $_POST['oldpw']);
    $newpw = hash("whirlpool", $_POST['newpw']);
    ft_modify_account($login, $oldpw, $newpw);
}
else
    echo "ERROR\n";
?>